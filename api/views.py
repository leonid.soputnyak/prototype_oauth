from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser

from api.models import User
from api.permissions import IsLoggedInUserOrAdmin
from api.serializer import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action in ('create', 'destroy'):
            permission_classes = [IsAdminUser]
        elif self.action in ('retrieve', 'update', 'partial_update', 'list'):
            permission_classes = [IsLoggedInUserOrAdmin]
        return [permission() for permission in permission_classes]
