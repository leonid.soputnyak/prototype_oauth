Prerequisites <br/>
For getting all neded libraries just use
`pip install -r requirements.txt `
from root directory


Running development server <br/>
For running dev server just use
`python manage.py runserver`
from root directory


endpoints<br/>
http://127.0.0.1:8000/auth/google/ - google oauth2 api. Not implement now <br/>
http://127.0.0.1:8000/api/v1/ - API root. <br/>
http://127.0.0.1:8000/api/v1/auth/login/ - endpoint for login with provided credentials. <br/>
http://127.0.0.1:8000/api/v1/users/ - endpoint for getting user list. <br/>
http://127.0.0.1:8000/api/v1/users/1/ - endpoint for getting user detail. If current user logged as not administarator, and want get detail about another user, it will be  HTTP 403 Forbidden with detail": "You do not have permission to perform this action."
". <br/>
http://127.0.0.1:8000/api/v1/auth/logout/ - endpoint for logout <br/>

users for quick test:

admin <br/>

{
    "username": "testadmin",
    "email": "testadmin@gmail.com",
    "password": "1"
}

user <br/>

{
    "username": "testuser",
    "email": "some_test_user_with_no_admin_permissions@gmail.com",
    "password": "1"
}